# example

- app/router.js 用于配置 URL 路由规则，具体参见 Router。
- app/controller/** 用于解析用户的输入，处理后返回相应的结果，具体参见 Controller。
- app/service/** 用于编写业务逻辑层，可选，建议使用，具体参见 Service。
- app/middleware/** 用于编写中间件，可选，具体参见 Middleware。
- app/public/** 用于放置静态资源，可选，具体参见内置插件 egg-static。
- app/extend/** 用于框架的扩展，可选，具体参见框架扩展。
- config/config.{env}.js 用于编写配置文件，具体参见配置。
- config/plugin.js 用于配置需要加载的插件，具体参见插件。
- test/** 用于单元测试，具体参见单元测试。
- app.js 和 agent.js 用于自定义启动时的初始化工作，可选，具体参见启动自定义。关于agent.js的作用参见Agent机制。

由内置插件约定的目录：
- app/public/** 用于放置静态资源，可选，具体参见内置插件 egg-static。
- app/schedule/** 用于定时任务，可选，具体参见定时任务。
若需自定义自己的目录规范，参见 Loader API
- app/view/** 用于放置模板文件，可选，由模板插件约定，具体参见模板渲染。
- app/model/** 用于放置领域模型，可选，由领域类相关插件约定，如 egg-sequelize。

## 快速入门

<!-- 在此次添加使用文档 -->

如需进一步了解，参见 [egg 文档][egg]。

### 本地开发

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### 部署

```bash
$ npm start
$ npm stop
```

### 单元测试

- [egg-bin] 内置了 [mocha], [thunk-mocha], [power-assert], [istanbul] 等框架，让你可以专注于写单元测试，无需理会配套工具。
- 断言库非常推荐使用 [power-assert]。
- 具体参见 [egg 文档 - 单元测试](https://eggjs.org/zh-cn/core/unittest)。

### 内置指令

- 使用 `npm run lint` 来做代码风格检查。
- 使用 `npm test` 来执行单元测试。
- 使用 `npm run autod` 来自动检测依赖更新，详细参见 [autod](https://www.npmjs.com/package/autod) 。


[egg]: https://eggjs.org

### router
将用户的请求基于 method 和 URL 分发到了对应的 Controller 上

### controller
解析用户的输入，处理后返回相应的结果

1. 获取用户通过 HTTP 传递过来的请求参数。
2. 校验、组装参数。
3. 调用 Service 进行业务处理，必要时处理转换 Service 的返回结果，让它适应用户的需求。
4. 通过 HTTP 将结果响应给用户。

this属性
- this.ctx: 当前请求的上下文 Context 对象的实例，通过它我们可以拿到框架封装好的处理当前请求的各种便捷属性和方法。
- this.app: 当前应用 Application 对象的实例，通过它我们可以拿到框架提供的全局对象和方法。
- this.service：应用定义的 Service，通过它我们可以访问到抽象出的业务层，等价于 this.ctx.service 。
- this.config：应用运行时的配置项。
- this.logger：logger 对象，上面有四个方法（debug，info，warn，error），分别代表打印四个不同级别的日志，使用方法和效果与 context logger 中介绍的一样，但是通过这个 logger 对象记录的日志，在日志前面会加上打印该日志的文件路径，以便快速定位日志打印位置。


### node实现多进程 ==》 Node.js 官方提供的解决方案是 Cluster 模块
- 负责启动其他进程的叫做 Master 进程，他好比是个『包工头』，不做具体的工作，只负责启动其他进程。 （分发管理）
- 其他被启动的叫 Worker 进程，顾名思义就是干活的『工人』。它们接收请求，对外提供服务。 （具体业务）
- Worker 进程的数量一般根据服务器的 CPU 核数来定，这样就可以完美利用多核资源。 （根据核心数控制进程数量）
```javascript 1.8
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  http.createServer(function(req, res) {
    res.writeHead(200);
    res.end("hello world\n");
  }).listen(8000);
}
```

- Worker 进程异常退出以后该如何处理？
- 多个 Worker 进程之间如何共享资源？
- 多个 Worker 进程之间如何调度？

###  Agent 机制 单进程任务使用（统计汇总等，类似秘书）
1. 每天凌晨 0 点，将当前日志文件按照日期进行重命名
2. 销毁以前的文件句柄，并创建新的日志文件继续写入
```
              +--------+          +-------+
                | Master |<-------->| Agent |
                +--------+          +-------+
                ^   ^    ^
               /    |     \
             /      |       \
           /        |         \
         v          v          v
+----------+   +----------+   +----------+
| Worker 1 |   | Worker 2 |   | Worker 3 |
+----------+   +----------+   +----------+
```

那我们框架的启动时序如下：
```
+---------+           +---------+          +---------+
|  Master |           |  Agent  |          |  Worker |
+---------+           +----+----+          +----+----+
     |      fork agent     |                    |
     +-------------------->|                    |
     |      agent ready    |                    |
     |<--------------------+                    |
     |                     |     fork worker    |
     +----------------------------------------->|
     |     worker ready    |                    |
     |<-----------------------------------------+
     |      Egg ready      |                    |
     +-------------------->|                    |
     |      Egg ready      |                    |
     +----------------------------------------->|
```

1. Master 启动后先 fork Agent 进程
2. Agent 初始化成功后，通过 IPC 通道通知 Master
3. Master 再 fork 多个 App Worker
4. App Worker 初始化成功，通知 Master
5. 所有的进程初始化成功后，Master 通知 Agent 和 Worker 应用启动成功

另外，关于 Agent Worker 还有几点需要注意的是：
1. 由于 App Worker 依赖于 Agent，所以必须等 Agent 初始化完成后才能 fork App Worker
2. Agent 虽然是 App Worker 的『小秘』，但是业务相关的工作不应该放到 Agent 上去做，不然把她累垮了就不好了
3. 由于 Agent 的特殊定位，我们应该保证它相对稳定。当它发生未捕获异常，框架不会像 App Worker 一样让他退出重启，而是记录异常日志、报警等待人工处理
4. Agent 和普通 App Worker 挂载的 API 不完全一样，如何识别差异可查看框架文档
```
// agent.js
module.exports = agent => {
  // 在这里写你的初始化逻辑

  // 也可以通过 messenger 对象发送消息给 App Worker
  // 但需要等待 App Worker 启动成功后才能发送，不然很可能丢失
  agent.messenger.on('egg-ready', () => {
    const data = { ... };
    agent.messenger.sendToApp('xxx_action', data);
  });
};
代码会执行在 agent 进程上
```
桥梁：通过框架封装的 messenger 对象进行进程间通讯（IPC）
```
代码会执行在 Worker 进程上
// app.js
module.exports = app => {
  app.messenger.on('xxx_action', data => {
    // ...
  });
};
```
```
类型	       进程数量	            作用	                        稳定性	是否运行业务代码
Master	   1	                进程管理，进程间消息转发	    非常高	否
Agent	   1	                后台运行工作（长连接客户端）	高	    少量
Worker	   一般设置为 CPU 核数	执行业务代码	                一般  	是
```
进程间通信
```
广播消息： agent => all workers
                  +--------+          +-------+
                  | Master |<---------| Agent |
                  +--------+          +-------+
                 /    |     \
                /     |      \
               /      |       \
              /       |        \
             v        v         v
  +----------+   +----------+   +----------+
  | Worker 1 |   | Worker 2 |   | Worker 3 |
  +----------+   +----------+   +----------+

指定接收方： one worker => another worker
                  +--------+          +-------+
                  | Master |----------| Agent |
                  +--------+          +-------+
                 ^    |
     send to    /     |
    worker 2   /      |
              /       |
             /        v
  +----------+   +----------+   +----------+
  | Worker 1 |   | Worker 2 |   | Worker 3 |
  +----------+   +----------+   +----------+
```
- app.messenger.broadcast(action, data)：发送给所有的 agent / app 进程（包括自己）
- app.messenger.sendToApp(action, data): 发送给所有的 app 进程
  - 在 app 上调用该方法会发送给自己和其他的 app 进程
  - 在 agent 上调用该方法会发送给所有的 app 进程
- app.messenger.sendToAgent(action, data): 发送给 agent 进程
  - 在 app 上调用该方法会发送给 agent 进程
  - 在 agent 上调用该方法会发送给 agent 自己
- agent.messenger.sendRandom(action, data):
  - app 上没有该方法（现在 Egg 的实现是等同于 sentToAgent）
  - agent 会随机发送消息给一个 app 进程（由 master 来控制发送给谁）
- app.messenger.sendTo(pid, action, data): 发送给指定进程
```
// app.js
module.exports = app => {
  // 注意，只有在 egg-ready 事件拿到之后才能发送消息
  app.messenger.once('egg-ready', () => {
    app.messenger.sendToAgent('agent-event', { foo: 'bar' });
    app.messenger.sendToApp('app-event', { foo: 'bar' });
  });
}
```

### 插件引用
1. {Boolean} enable - 是否开启此插件，默认为 true
2. {String} package - npm 模块名称，通过 npm 模块形式引入插件
3. {String} path - 插件绝对路径，跟 package 配置互斥
4. {Array} env - 只有在指定运行环境才能开启，会覆盖插件自身 package.json 中的配置
```
// config/plugin.js
// 使用 mysql 插件
exports.mysql = {
  enable: true,
  package: 'egg-mysql',
};
```
```
一般用于自定义插件
// config/plugin.js
const path = require('path');
exports.mysql = {
  enable: true,
  package: path.join(__dirname, '../lib/plugin/egg-mysql'),
};
```
插件一般会包含自己的默认配置，应用开发者可以在 config.default.js 覆盖对应的配置：
```
// config/config.default.js
exports.mysql = {
  client: {
    host: 'mysql.com',
    port: '3306',
    user: 'test_user',
    password: 'test_password',
    database: 'test',
  },
};
```

### MySQL 事务
主要用于处理操作量大，复杂度高的数据

一般来说，事务是必须满足4个条件（ACID）： Atomicity（原子性）、Consistency（一致性）、Isolation（隔离性）、Durability（可靠性）
- 原子性：确保事务内的所有操作都成功完成，否则事务将被中止在故障点，以前的操作将回滚到以前的状态。
- 一致性：对于数据库的修改是一致的。
- 隔离性：事务是彼此独立的，不互相影响
- 持久性：确保提交事务后，事务产生的结果可以永久存在。

对于一个事务来讲，一定伴随着 beginTransaction、commit 或 rollback，分别代表事务的开始，成功和失败回滚。

### 加载单元
```
loadUnit
├── package.json
├── app.js
├── agent.js
├── app
│   ├── extend
│   |   ├── helper.js
│   |   ├── request.js
│   |   ├── response.js
│   |   ├── context.js
│   |   ├── application.js
│   |   └── agent.js
│   ├── service
│   ├── middleware
│   └── router.js
└── config
    ├── config.default.js
    ├── config.prod.js
    ├── config.test.js
    ├── config.local.js
    └── config.unittest.js
```

###框架基础对象
Application, Context, Request, Response
###框架扩展对象
Controller, Service, Helper, Config, Logger
###Context
Context 是一个请求级别的对象，继承自 Koa.Context。
在每一次收到用户请求时，框架会实例化一个 Context 对象，这个对象封装了这次用户请求的信息，并提供了许多便捷的方法来获取请求参数或者设置响应信息。
框架会将所有的 Service 挂载到 Context 实例上，一些插件也会将一些其他的方法和对象挂载到它上面（egg-sequelize 会将所有的 model 挂载在 Context 上）。

###  Helper
工具函数

### 环境配置
config.default.js一定会加载，当指定 env 时会同时加载对应的配置文件，并覆盖默认配置文件的同名配置。

```
config
|- config.default.js
|- config.prod.js
|- config.unittest.js
`- config.local.js
```

框架在启动时会把合并后的最终配置 dump 到 run/application_config.json（worker 进程）和 run/agent_config.json（agent 进程）中
可用于分析问题


