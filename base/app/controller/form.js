'use strict';

const Controller = require('egg').Controller;

class UserController extends Controller {
    async post() {
        const {
            ctx
        } = this;
        ctx.body = `body: ${JSON.stringify(ctx.request.body)}`;
    }
}

module.exports = UserController;