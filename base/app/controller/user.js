'use strict';

const Controller = require('egg').Controller;

class UserController extends Controller {
    async info() {
        const {
            ctx
        } = this;
        // ctx.body = {
        //     name: `hello ${ctx.params.id}`,
        // };
        // 调用服务查询详细信息
        const userId = ctx.params.id;
        const userInfo = await ctx.service.user.find(userId);
        ctx.body = userInfo;
    }
}

module.exports = UserController;