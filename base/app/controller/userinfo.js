'use strict';

const Controller = require('egg').Controller;

const createRule = {
    username: {
        type: 'email',
    },
    password: {
        type: 'password',
        compare: 're-password',
    },
};


class UserInfoController extends Controller {

    async user() {
        const {
            ctx
        } = this;
        // 如果校验报错，会抛出异常
        ctx.validate(createRule);
        ctx.body = ctx.request.body;
    }
}

module.exports = UserInfoController;