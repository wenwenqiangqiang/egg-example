# 获取http参数
- ctx.query 查询字符串，重复的值会只取第一个值
```
防止恶意请求通常会过滤掉无效值
const key = ctx.query.key || '';
if (key.startsWith('egg')) {
  // do something
}
```
- ctx.queries 相同值会保存到一个数组中，GET /posts?category=egg&id=1&id=2&id=3
```
class PostController extends Controller {
  async listPosts() {
    console.log(this.ctx.queries);
    // {
    //   category: [ 'egg' ],
    //   id: [ '1', '2', '3' ],
    // }
  }
}
```
- ctx.request.body 请求体内容 Content-Type 为json，form 进行不同解析

```
config/config.default.js中配置限制大小，默认100kb
module.exports = {
  bodyParser: {
    jsonLimit: '1mb',
    formLimit: '1mb',
  },
};
```

- ctx.response.body 相应体，可简写为ctx.body

- ctx.headers 都是获取整个 header 对象

- ctx.get(name) 获取请求 header 中的一个字段的值，如果这个字段不存在，会返回空字符串

- ctx.protocol 是 HTTP 还是 https

- ctx.service 调用service方法

