'use strict';

/**
 * @param {Egg.Application} app - egg application
 *
 */

module.exports = app => {
    const {
        router,
        controller
    } = app;
    router.get('/', controller.home.index);
    // 路径及控制器中间可以加中间件
    router.get('/user/:id', controller.user.info);
    // 处理查询字符串
    router.get('/search', app.controller.search.index);
    // post请求
    router.post('/form', app.controller.form.post);
    // 表单校验
    router.post('/userinfo', app.controller.userinfo);
    // 重定向
    router.redirect('/home/index', '/', 302);
    // 使用中间件
    router.get('s', '/searchBig', app.middlewares.uppercase(), app.controller.search.index)
};