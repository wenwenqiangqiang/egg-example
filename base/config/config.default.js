'use strict';

module.exports = appInfo => {
    // appInfo包含应用的全部信息
    const config = exports = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_1536649034417_3620';

    // add your config here
    config.middleware = ['gzip'];
    // 配置 gzip 中间件的配置
    config.gzip = {
        threshold: 1024, // 小于 1k 的响应体不压缩
    };

    config.validate = {
        // convert: false,
        // validateRoot: false,
    };

    // 关闭安全校验，默认开启,实际项目中建议开启
    config.security = {
        csrf: false
    };
    // 设置请求体body长度
    config.bodyParser = {
        jsonLimit: '1mb',
        formLimit: '1mb',
    };

    config.mysql = {
        // 单数据库信息配置
        client: {
            // host
            host: 'mysql.com',
            // 端口号
            port: '3306',
            // 用户名
            user: 'test_user',
            // 密码
            password: 'test_password',
            // 数据库名
            database: 'test',
        },
        // 是否加载到 app 上，默认开启
        app: true,
        // 是否加载到 agent 上，默认关闭
        agent: false,
    };


    return config;
};

