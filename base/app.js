// 项目启动初始化操作（可选）Application继承自 Koa.Application，只实例化一次
module.exports = app => {

    // app.cache = new Cache();


    app.once('server', server => {
        // 初始化完成事件，只执行一次
        // websocket
    });
    app.on('error', (err, ctx) => {
        // 全局异常捕获
        // report error
    });
    app.on('request', ctx => {
        // 请求统一拦截
        // log receive request
    });
    app.on('response', ctx => {
        // 请求统一响应
        // ctx.starttime is set by framework
        const used = Date.now() - ctx.starttime;
        // log total cost
    });


    /**
     * 启动脚本，启动前执行操作
     * 注意：在 beforeStart 中不建议做太耗时的操作，框架会有启动的超时检测。
     */
    app.beforeStart(async () => {
        // 应用会等待这个函数执行完成才启动
        // app.cities = await app.curl('http://example.com/city.json', {
        //     method: 'GET',
        //     dataType: 'json',
        // });

        // 也可以通过以下方式来调用 Service
        // const ctx = app.createAnonymousContext(); 以匿名的方式创建一个上下文实例，然后调用service方法加载
        // app.cities = await ctx.service.cities.load();
    });
};